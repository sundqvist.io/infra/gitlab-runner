#!/bin/bash

KUBECTL_DIR=~/kubectl/$KUBECTL_VERSION/
KUBECTL=$KUBECTL_DIR/kubectl

if [ ! -f $KUBECTL ]; then
  mkdir -p $KUBECTL_DIR && pushd $KUBECTL_DIR
  KUBECTL_URL=https://storage.googleapis.com/kubernetes-release/release/$KUBECTL_VERSION/bin/linux/amd64/kubectl
  echo Downloading $KUBECTL_URL
  curl -LO $KUBECTL_URL
  chmod +x ./kubectl && popd
fi

$KUBECTL "$@"
