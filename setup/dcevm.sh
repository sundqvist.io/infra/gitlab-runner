mkdir /tmp/dcevm && \
    curl -L -o /tmp/dcevm/%DCEVM_INSTALLER_NAME% "%DCEVM_INSTALLER_URL%" && \

cd /tmp/dcevm && \
    unzip %DCEVM_INSTALLER_NAME% && \
    mkdir -p /opt/jdk/jre/lib/amd64/dcevm && \
    cp linux_amd64_compiler2/product/libjvm.so /opt/jdk/jre/lib/amd64/dcevm/libjvm.so && \

    mkdir -p /opt/hotswap-agent/ && \
    curl -L -o /opt/hotswap-agent/hotswap-agent-${HOTSWAP_AGENT_VERSION}.jar "https://github.com/HotswapProjects/HotswapAgent/releases/download/RELEASE-${HOTSWAP_AGENT_VERSION}/hotswap-agent-${HOTSWAP_AGENT_VERSION}.jar" && \
    ln -s /opt/hotswap-agent/hotswap-agent-${HOTSWAP_AGENT_VERSION}.jar /opt/hotswap-agent/hotswap-agent.jar && \
