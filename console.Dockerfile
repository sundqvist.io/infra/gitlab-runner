FROM bitnami/minideb

RUN apt-get update && install_packages -y nano curl wget unzip zip tree git zsh jq \
    apt-transport-https ca-certificates gettext-base build-essential zlib1g-dev gcc && \
    echo 'set linenumbers' >> ~/.nanorc && \
    echo 'set tabsize 4' >> ~/.nanorc && \
    git config --global pull.rebase true
SHELL ["zsh", "-ic"]
RUN update-ca-certificates && \
    curl -fsSL https://raw.githubusercontent.com/zimfw/install/master/install.zsh | zsh && source "$HOME/.zim/init.zsh" && \
    echo 'zmodule https://github.com/jsundqvist/bastard.zsh-theme -n bastard' >> "$HOME/.zimrc"
RUN zimfw install

#RUN export IMG_SHA256="41aa98ab28be55ba3d383cb4e8f86dceac6d6e92102ee4410a6b43514f4da1fa" && \
#    curl -fSL "https://github.com/genuinetools/img/releases/download/v0.5.7/img-linux-amd64" -o "/usr/local/bin/img" && \
#    echo "${IMG_SHA256}  /usr/local/bin/img" | sha256sum -c - && chmod a+x "/usr/local/bin/img"

ARG DOCKER_VERSION
ENV DOCKER_VERSION $DOCKER_VERSION
RUN curl -fsSL https://download.docker.com/linux/static/stable/x86_64/docker-$DOCKER_VERSION.tgz | \
    tar zxvf - --strip 1 -C /usr/bin docker/docker
ENV DOCKER_BUILDKIT=1

RUN LATEST=$(wget -qO- "https://api.github.com/repos/docker/buildx/releases/latest" | jq -r .name) && \
    wget https://github.com/docker/buildx/releases/download/$LATEST/buildx-$LATEST.linux-amd64 && \
    chmod a+x buildx-$LATEST.linux-amd64 && \
    mkdir -p ~/.docker/cli-plugins && \
    mv buildx-$LATEST.linux-amd64 ~/.docker/cli-plugins/docker-buildx && \
    docker buildx install

ARG KUBECTL_VERSION
ENV KUBECTL_VERSION $KUBECTL_VERSION
COPY kubectl.sh /usr/local/bin/kubectl
RUN echo "source <(kubectl completion zsh)" >> ~/.zshrc

CMD ["zsh"]
