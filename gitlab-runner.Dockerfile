FROM registry.gitlab.com/sundqvist.io/infra/gitlab-runner/console

ARG KUBECTL_VERSION
ENV KUBECTL_VERSION $KUBECTL_VERSION

RUN curl -s "https://get.sdkman.io" | bash && source "$HOME/.sdkman/bin/sdkman-init.sh" && \
    echo "source $HOME/.sdkman/bin/sdkman-init.sh" >> "$HOME/.profile"

RUN sdk install java 21.0.2-graalce #$(sdk list java | grep 'GraalVM' | grep -o '[^|]*$') && \
    sdk install gradle 8.5
    
ENV PATH="/root/.sdkman/candidates/java/current/bin:/root/.sdkman/candidates/gradle/current/bin:${PATH}"

RUN curl -L --output /usr/local/bin/gitlab-runner https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-linux-amd64 && \
    chmod +x /usr/local/bin/gitlab-runner && \
    useradd --comment 'GitLab Runner' --create-home gitlab-runner && \
    gitlab-runner install --user=gitlab-runner --working-directory=/home/gitlab-runner

ADD --chown=gitlab-runner:gitlab-runner config.toml /etc/gitlab-runner/

CMD gitlab-runner register \
      --non-interactive \
      --url "https://gitlab.com/" \
      --registration-token "$REGISTRATION_TOKEN" \
      --executor "shell" && \
    gitlab-runner --debug run
